program M3U8Download;

uses
  Vcl.Forms,
  FM_Main in 'FM_Main.pas' {frmMain};

{$R *.res}

begin
  Application.Initialize;
  ReportMemoryLeaksOnShutdown := True;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.

