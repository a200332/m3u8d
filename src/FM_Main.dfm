object frmMain: TfrmMain
  Left = 0
  Top = 0
  Caption = 'M3U8'#35270#39057#19979#36733
  ClientHeight = 430
  ClientWidth = 602
  Color = clBtnFace
  Font.Charset = GB2312_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #24494#36719#38597#40657
  Font.Style = []
  OldCreateOrder = False
  WindowState = wsMaximized
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  DesignSize = (
    602
    430)
  PixelsPerInch = 96
  TextHeight = 17
  object lblTProgress: TLabel
    Left = 519
    Top = 78
    Width = 19
    Height = 17
    Anchors = [akTop, akRight]
    Caption = '0/0'
  end
  object lbl1: TLabel
    Left = 13
    Top = 78
    Width = 48
    Height = 17
    Caption = #19979#36733#36827#24230
  end
  object edtURL: TLabeledEdit
    Left = 64
    Top = 16
    Width = 449
    Height = 25
    Anchors = [akLeft, akTop, akRight]
    EditLabel.Width = 48
    EditLabel.Height = 17
    EditLabel.Caption = #19979#36733#22320#22336
    LabelPosition = lpLeft
    TabOrder = 0
  end
  object btnRun: TButton
    Left = 519
    Top = 16
    Width = 75
    Height = 56
    Anchors = [akTop, akRight]
    Caption = #24320#22987#19979#36733
    TabOrder = 1
    OnClick = btnRunClick
  end
  object mmoLog: TMemo
    Left = 8
    Top = 112
    Width = 586
    Height = 310
    Anchors = [akLeft, akTop, akRight, akBottom]
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 5
  end
  object edtFile: TLabeledEdit
    Left = 64
    Top = 47
    Width = 372
    Height = 25
    Anchors = [akLeft, akTop, akRight]
    EditLabel.Width = 48
    EditLabel.Height = 17
    EditLabel.Caption = #20445#23384#20301#32622
    LabelPosition = lpLeft
    ReadOnly = True
    TabOrder = 2
  end
  object btnSelectFile: TButton
    Left = 442
    Top = 47
    Width = 71
    Height = 25
    Anchors = [akTop, akRight]
    Caption = #36873#25321#20301#32622
    TabOrder = 3
    OnClick = btnSelectFileClick
  end
  object pb1: TProgressBar
    Left = 64
    Top = 78
    Width = 449
    Height = 17
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 4
  end
  object dlgSave1: TSaveDialog
    Options = [ofHideReadOnly, ofPathMustExist, ofEnableSizing]
    Left = 264
    Top = 208
  end
end
